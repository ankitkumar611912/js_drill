// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function getAllCarsOlderThan2000(getAllYears){
    if(Array.isArray(getAllYears)){
        let getCarOlderThan2000 = [];
        for(let val of getAllYears){
            if(val < 2000){
                getCarOlderThan2000.push(val);
            }
        }
        return getCarOlderThan2000;
    }
    else{
        return [];
    }

}

module.exports = getAllCarsOlderThan2000;