// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"


function getLastCarInfo(data){
    if(Array.isArray(data) && data.length > 0) {
        let lastCar = data[data.length - 1];
        return `Last car is a ${lastCar.car_make} ${lastCar.car_model}.`
    } else {
        return ""; 
    }
}

module.exports = getLastCarInfo;
