// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


function getBMWAUDI(data){
    if(Array.isArray(data)){
        let getAllBMWAndAudi  = [];
        for(let val of data){
            if(val.car_make === 'BMW' || val.car_make === 'Audi'){
                getAllBMWAndAudi.push(val);
            }
        }
        return getAllBMWAndAudi;
    }else{
        return [];
    }
}

module.exports = getBMWAUDI;