// ==== Problem #1 ====
// The dealer can't recall the information for a car with an id of 33 on his lot. Help the dealer find out which car has an id of 33 by calling a function that will return the data for that car. Then log the car's year, make, and model in the console log in the format of:
//"Car 33 is a *car year goes here* *car make goes here* *car model goes here*"

//"id":33,"car_make":"Jeep","car_model":"Wrangler","car_year":2011},

function getCarId(data){
    if(Array.isArray(data)){
        for(let val of data){
            if(val.id == 33){
                return `Car 33 is a ${val.car_year} ${val.car_make} ${val.car_model}.`;
            }
        }
    }
    else
        return "";
}

module.exports = getCarId;