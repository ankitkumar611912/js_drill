//The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortingAlphabetically(sortedCarModel){
    if(Array.isArray(sortedCarModel)){
        let n = sortedCarModel.length;
        let swapped;
        do {
            swapped = false;
            for (let index = 1; index < n; index++) {
                if (sortedCarModel[index - 1] > sortedCarModel[index]) {
                    // Swap the elements
                    let temp = sortedCarModel[index - 1];
                    sortedCarModel[index - 1] = sortedCarModel[index];
                    sortedCarModel[index] = temp;
                    swapped = true;
                }
            }
        } while (swapped);
        return sortedCarModel
    }
    else{
        return [];
    }
}

function sortCarModelsAlphabetically(data){
    if(Array.isArray(data)){
        let sortedCarModel = [];
        for(let val of data){
            sortedCarModel.push(val.car_model);
        }
        sortedCarModel = sortingAlphabetically(sortedCarModel);
        return sortedCarModel;
    }
    else{
        return [];
    }
}
module.exports = sortCarModelsAlphabetically;