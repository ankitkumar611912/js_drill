// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function getAllYears(data){
    if(Array.isArray(data)){
        let allYears = [];
        for(let val of data){
            allYears.push(val.car_year);
        }
        return allYears;
    }
    else{
        return [];
    }
}

module.exports = getAllYears;